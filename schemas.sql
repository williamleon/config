CREATE SCHEMA IF NOT EXISTS userschema;
CREATE USER IF NOT EXISTS 'myuser'@'%' IDENTIFIED BY 'myuserpwd';
GRANT ALL PRIVILEGES ON userschema.* TO 'myuser'@'%';

CREATE SCHEMA IF NOT EXISTS productschema;
CREATE USER IF NOT EXISTS 'productuser'@'%' IDENTIFIED BY 'productuserpwd';
GRANT ALL PRIVILEGES ON productschema.* TO 'productuser'@'%';

CREATE SCHEMA IF NOT EXISTS purchaseorderschema;
CREATE USER IF NOT EXISTS 'purchaseorderschemauser'@'%' IDENTIFIED BY 'purchaseorderschemapwd';
GRANT ALL PRIVILEGES ON purchaseorderschema.* TO 'purchaseorderschemauser'@'%';

CREATE SCHEMA IF NOT EXISTS paymentschema;
CREATE USER IF NOT EXISTS 'paymentuser'@'%' IDENTIFIED BY 'paymentpwd';
GRANT ALL PRIVILEGES ON paymentschema.* TO 'paymentuser'@'%';
